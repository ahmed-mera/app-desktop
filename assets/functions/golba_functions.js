/**
 * export functions wii be use
 * */
const loading = document.querySelector('.loading');

export { toggleDisable, checkEmail, redirect, toggleLoading, loading }

/**
 * to toggle disable
 * @param param ( html parm )
 * it 'll toggle disabled and add class disable
 * */
function toggleDisable(param) {
    param.disabled = !param.disabled;
    param.classList.toggle('disable');
}

/**
 * checkEmail
 * @param input ( html parm )
 * check if it's a vaild email or not
 * @return true or false
 * */
function checkEmail(input) {
    const val = input.value;
    if (!((/\w{5,}@+\w{5,7}.+\w{2,3}/gim.test(val)) && (!/\s/.test(val))) || val === "") {
        input.classList.add('invalid');
        input.nextElementSibling.style.display = 'block';
        return false;
    } else {
        input.classList.remove('invalid');
        input.nextElementSibling.style.display = 'none';
        return true;
    }
}

/**
 * this function redirect the client where you want if you pass the path as param
 * @param path is optional
 * */
function redirect(path = '../index.html') {
    window.location = path;
}


function toggleLoading() {
    loading.classList.toggle('show');
}
