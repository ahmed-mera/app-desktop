import * as functions from "../assets/functions/golba_functions.js"

let register = document.getElementById('register'),
    login = document.getElementById('login'),
    xhttp = new XMLHttpRequest(),
    email = document.getElementById("email"),
    username = document.getElementById("username"),
    password = document.getElementById("password"),
    confirmPassword = document.getElementById("confirm_password");

const msg = document.getElementsByClassName('error')[0];


register.onclick = function(){
    if(functions.checkEmail(email) && check(username, 5 , /[^\w\s]|\d/gim) &&
        check(password, 6) &&  equal(password, confirmPassword) ){

        functions.toggleLoading();

        functions.toggleDisable(register);

        const data = `username=${username.value}&email=${email.value}&password=${password.value}`;

        xhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 201) {
                showMsg (JSON.parse(this.responseText));
            }
        };
        xhttp.open("POST", "https://mera.ddns.net/API/add.client.php", true);
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhttp.send(data);
    }
}


function check (input, length, pattern = /\W/gim){
    const val  = input.value;
    if(val === "" || val.match(pattern) || val.length < length ){
        input.classList.add('invalid');
        input.nextElementSibling.style.display = 'block';
        return false;
    }else{
        input.classList.remove('invalid');
        input.nextElementSibling.style.display = 'none';
        return true;
    }
}

function equal(pwd, confPwd) {
    if (pwd.value === confPwd.value){
        confPwd.classList.remove('invalid');
        confPwd.nextElementSibling.style.display = 'none';
        return true;
    }else {
        confPwd.classList.add('invalid');
        confPwd.nextElementSibling.style.display = 'block';
        return false;
    }
}




function showMsg (res){

    functions.toggleLoading();

    (res?.error ) ? msg.innerHTML = res.error : functions.redirect( '../index.html');

    msg.style.display = 'block';

    setTimeout(() => {
        msg.style.display = 'none';
    },10000)
}


login.onclick = () => {
    functions.redirect( '../index.html');
}
