import * as functions from "../assets/functions/golba_functions.js"

let login = document.getElementById('login'),
    xhttp = new XMLHttpRequest() ,
    email = document.getElementById("email"),
    password = document.getElementById("password");

const msg = document.getElementsByClassName('error')[0];

login.onclick = () => {


  if(check(email, /^\w\s]|\d/gmi) && check(password)){
    functions.toggleLoading();
    functions.toggleDisable(login);
    xhttp.onreadystatechange = function() {
      if (this.readyState === 4 && this.status === 200) {
        showMsg(JSON.parse(this.responseText));
      }
    };
    xhttp.open("POST", "https://mera.ddns.net/API/get.client.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(`email=${email.value}&password=${password.value}`);
  }

}


function check (input, pattern = /\W/gim ){
  const val  = input.value;
  if(val === "" || val.match(pattern)){
    input.classList.add('invalid');
    return false;
  }else{
    input.classList.remove('invalid');
    return true;
  }
}



function showMsg (res){

  functions.toggleLoading();

  (res?.error) ? msg.innerHTML = res.error : functions.redirect( './home/home.html');

  msg.style.display = 'block';

  functions.toggleDisable(login);

  setTimeout(() => {
    msg.style.display = 'none';
  },5000)

}

document.getElementById('register').onclick = () => {
  functions.redirect( './register/register.html');
}

